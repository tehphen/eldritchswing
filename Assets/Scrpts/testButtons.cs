﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class testButtons : MonoBehaviour, IPointerDownHandler
{
    public UiUpdate UpdateScript;
    // Start is called before the first frame update
    void Start()
    {

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnPointerDown(PointerEventData eventData) 
    {
        Debug.Log("Clicked");
        UpdateScript.UpdatePopulation(10, 50);
        UpdateScript.UpdateResource1(15, 50);
        UpdateScript.UpdateResource2(25, 50);
        UpdateScript.UpdateResource3(48, 50);

    }
}
